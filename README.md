Get HiLive
-----------

Please visit https://gitlab.com/SimonHTausch/HiLive.  
   
There you can find the latest version of HiLive, source code, documentation and
examples.